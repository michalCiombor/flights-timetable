import React from 'react';
import logo from '../../grap/logo.svg';
import './Header.css';



const Header = (props) => {
    return (
        <div className="header">
            <div className="radio">
                <input onChange={() => props.onChange("return")} type="radio" name="return" id="Return" checked={!props.isActive} />
                <label style={{ display: "inline" }} htmlFor="Return">Return</label>
                <input onClick={() => props.onChange("oneWay")} type="radio" name="return" id="OneWay" />
                <label style={{ display: "inline" }} htmlFor="OneWay">One Way</label>
            </div>

            <img src={logo} alt="lot logo" />

        </div>
    )
}

export default Header;