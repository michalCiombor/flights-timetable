import React from "react";
import Header from "../Header/Header";
import Inputs from "../Inputs/Inputs";
import Passengers from "../Passengers/Pasengers";
import SelectClass from "../SelectClass/SelectClass";

import "./App.css";

class App extends React.Component {
  state = {
    origin: "",
    destination: "",
    active: false,
    oneWay: false,
    adults: 1,
    children: 0,
    classValue: "E",
    date: "",
    dateReturned: "",
    airports: [],
    airportOrigin: [],
    airportDeparture: [],
    focusOrigin: false,
    focusDeparture: false
  };

  /* setting up main link  */
  handleClickConnect = () => {
    const mainLink = "https://bookerproxy.lot.com/service.php?";
    let aporigin = this.state.origin.split("(");
    let origin = [aporigin[1].slice(0, 3)];
    let apdestination = this.state.destination.split("(");
    let destination = [apdestination[1].slice(0, 3)];
    let date = this.state.date.split("-").join("");
    let dateReturned = this.state.dateReturned.split("-").join("");
    let adult = this.state.adults;
    let children = this.state.children;
    let flightClass = this.state.classValue;

    let link = `${mainLink}COUNTRY_CODE=PL&LANGUAGE_CODE=pl&ORIGIN=${origin}&DESTINATION=${destination}&DEPARTURE_DATE=${date}${
      dateReturned ? `&${dateReturned}` : ""
    }&ADULT_COUNT=${adult}&CHILD_COUNT=${children}&PARTNER=L76OK50SI42D&CLASS=${flightClass}`;
    console.log(link);
    console.log(
      origin,
      destination,
      date,
      adult,
      children,
      flightClass,
      dateReturned
    );
    // console.log("request sent");
    window.location.href = link;
  };
  /* adding/removing passengers and childrens */
  handleStateChangeAdd = id => {
    if (id === "adults") {
      this.setState({
        adults: this.state.adults + 1
      });
      console.log(id);
    } else {
      this.setState({
        children: this.state.children + 1
      });
      console.log(id);
    }
  };
  /*flights class set */

  handleClassChange = e => {
    this.setState({
      classValue: e.target.value
    });
  };

  handleStateChangeSubstract = id => {
    if (id === "adults") {
      this.setState({
        adults: this.state.adults - 1
      });
    } else {
      this.setState({
        children: this.state.children - 1
      });
    }
  };
  //handle focus and disable list of airports

  handleInputFocus = id => {
    // if (id === "origin") {
    //   this.setState({
    //     focusOrigin: false
    //   });
    // } else if (id === "departure") {
    //   this.setState({
    //     focusDeparture: false
    //   });
    // }
  };
  //handle airport click//

  handleAirportClick = (el, id) => {
    if (id === "origin") {
      this.setState({
        origin: el,
        focusOrigin: false
      });
    } else if ((id = "departure")) {
      this.setState({
        destination: el,
        focusDeparture: false
      });
    }
  };

  /*users airport choice*/

  handleInputChange = e => {
    switch (e.target.name) {
      case "from": {
        this.setState({
          origin: e.target.value,
          focusOrigin: true
        });
        let value = this.state.origin.toLowerCase();
        const airportName = this.state.airports.map(
          el => `${el.airportName.toLowerCase()} (${el.airportCode})`
        );

        let filtered = airportName.filter(
          airport => airport.indexOf(value) > -1
        );

        this.setState({
          airportOrigin: [
            filtered.map(el => (
              <p
                key={el}
                onClick={() => this.handleAirportClick(`${el}`, "origin")}
              >
                {el}
              </p>
            ))
          ]
        });

        return filtered;
      }
      case "to": {
        this.setState({
          destination: e.target.value,
          focusDeparture: true
        });
        let value = this.state.destination.toLowerCase();
        const airportName = this.state.airports.map(
          el => `${el.airportName.toLowerCase()} (${el.airportCode})`
        );

        let filtered = airportName.filter(
          airport => airport.indexOf(value) > -1
        );

        this.setState({
          airportDeparture: [
            filtered.map(el => (
              <p
                key={el}
                onClick={() => this.handleAirportClick(`${el}`, "departure")}
              >
                {el}
              </p>
            ))
          ]
        });
        return filtered;
      }

      //Setting dates for flight

      case "date": {
        this.setState({
          date: e.target.value
        });
        break;
      }
      case "dateReturned": {
        this.setState({
          dateReturned: e.target.value
        });
        break;
      }
      default:
        return null;
    }
  };
  // FILTER TEST

  // handleInputChange = e => {
  //   this.setState({ origin: e.target.value });
  //   let value = this.state.origin.toLowerCase();
  //   const airportName = this.state.airports.map(el =>
  //     el.airportName.toLowerCase()
  //   );
  //   const airportCode = this.state.airports.map(el =>
  //     el.airportCode.toLowerCase()
  //   );

  //   let filtered = airportName.filter(airport => airport.indexOf(value) > -1);

  //   console.log(filtered);
  // };

  //setting one/returned journey

  handleRadioChange = e => {
    if (e.target === "return") {
      this.setState({
        oneWay: !this.state.oneWay
      });
    } else {
      this.setState({
        oneWay: !this.state.oneWay
      });
    }
  };

  //getting data from API//

  componentDidMount() {
    fetch("http://localhost:3000/lotniska.json")
      .then(response => response.json())
      .then(data =>
        this.setState({
          airports: data
        })
      )
      .catch(err => {
        console.log(err);
      });
  }
  //main render

  render() {
    return (
      <div className="mainApp">
        <Header
          isActive={this.state.oneWay}
          onChange={this.handleRadioChange}
        />
        <Inputs
          value={this.state}
          isActive={this.state.oneWay}
          change={this.handleInputChange}
          airports={this.state.airports}
          focus={this.handleInputFocus}
          destination={this.state.destination}
        />
        <div className="airportsOrigin">
          {this.state.focusOrigin ? this.state.airportOrigin : null}
        </div>
        <div className="airportsDeparture">
          {this.state.focusDeparture ? this.state.airportDeparture : null}
        </div>

        <div className="passengers">
          <div className="buttony">
            <div>
              <label htmlFor="adults">Adults</label>
              <Passengers
                id={"adults"}
                add={() => this.handleStateChangeAdd("adults")}
                substract={() => this.handleStateChangeSubstract("adults")}
                state={this.state.adults}
              />
            </div>
            <div>
              <label htmlFor="children">Children</label>
              <Passengers
                id="children"
                add={() => this.handleStateChangeAdd("children")}
                substract={() => this.handleStateChangeSubstract("children")}
                state={this.state.children}
              />
            </div>
          </div>
          <SelectClass
            change={this.handleClassChange}
            value={this.state.classValue}
          />
        </div>
        <input type="submit" value="Search" onClick={this.handleClickConnect} />
      </div>
    );
  }
}

export default App;
